package com.example.proyecto.ui.main;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Premierleague.class}, version = 1)
public abstract class Appsdatabase extends RoomDatabase {

    private static Appsdatabase INSTANCE;

    public static Appsdatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(
                            context.getApplicationContext(),
                            Appsdatabase.class, "db"
                    ).build();
        }
        return INSTANCE;
    }

    public abstract PremierLeagueDao getMatchesDao();
}