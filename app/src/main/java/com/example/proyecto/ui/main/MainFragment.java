package com.example.proyecto.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.camera2.TotalCaptureResult;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.proyecto.MainActivity;
import com.example.proyecto.R;

import java.util.ArrayList;

public class  MainFragment extends Fragment {

    private Premierleagueadapter adapter;
    public static String equiposelecionado;
    private PremierLeagueViewModel mViewModel;
    private SharedViewModel sharedModel;


    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        equiposelecionado=sp.getString("Plteams", "");

        Toast.makeText(getContext(), equiposelecionado, Toast.LENGTH_LONG).show();



        ListView lvprem = view.findViewById(R.id.lvpremier);

        lvprem.setOnItemClickListener((adapter, fragment, i, l) -> {
            Premierleague premierleague = (Premierleague) adapter.getItemAtPosition(i);

            if (!esTablet()) {
            Intent intent = new Intent(getContext(), Premier2.class);
            intent.putExtra("Match", premierleague);
            startActivity(intent);
            } else {
                sharedModel.select(premierleague);
            }
        });

        ArrayList<Premierleague> items= new ArrayList<>();
        adapter = new Premierleagueadapter(
                getContext(),
                R.layout.lv_prem_row,
                items
        );
        lvprem.setAdapter(adapter);

        mViewModel = ViewModelProviders.of(this).get(PremierLeagueViewModel.class);

        if(equiposelecionado.equals("0")) {
                mViewModel.getMatches().observe(getViewLifecycleOwner(), matches -> {
                adapter.clear();
                System.out.println("EQUIPOS DAO TODOS: "+matches);
                adapter.addAll(matches);
            });
        }else {
                mViewModel.getEquipoSelecionado().observe(getViewLifecycleOwner(), matches -> {
                System.out.println("equipos selecionado: "+equiposelecionado);
                adapter.clear();
                System.out.println("EQUIPOS DAO FROM SETTING: "+matches);
                adapter.addAll(matches);
            });
        }

        return view;
    }

    private boolean esTablet() {
        return getResources().getBoolean(R.bool.tablet);
    }

    public static String getEquipoSelect(){ return equiposelecionado; }


    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Premierleague>> implements com.example.proyecto.ui.main.RefreshDataTask {
        @Override
        protected ArrayList<Premierleague> doInBackground(Void... voids) {
            Premierleaguedbapi api = new Premierleaguedbapi();
            ArrayList<Premierleague> result = api.Getmatches();

            System.out.println("RESULT FROM API: "+result);

            Log.d("DEBUG", String.valueOf(result));

            return result;
        }


        @Override
        protected void onPostExecute(ArrayList<Premierleague> pl) {
            adapter.clear();
            for (Premierleague p : pl) {
                adapter.add(p);
            }
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_premier_fragment, menu);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        if (id == R.id.Setting_activity) {
            Intent i = new Intent(getContext(), SettingsActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
        mViewModel.reload();
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PremierLeagueViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }

}