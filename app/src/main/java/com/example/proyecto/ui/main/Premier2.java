package com.example.proyecto.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.proyecto.R;
import com.example.proyecto.ui.main.ui.main.Premier2Fragment;

public class Premier2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.premier2_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, Premier2Fragment.newInstance())
                    .commitNow();
        }
    }
}