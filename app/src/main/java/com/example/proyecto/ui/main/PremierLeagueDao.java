package com.example.proyecto.ui.main;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PremierLeagueDao {
    @Query("select * from Premierleague")
    LiveData<List<Premierleague>> getMatches();


    @Query("select * from Premierleague where team1 = :equipo or team2 = :equipo ")
    LiveData<List<Premierleague>> getEquipo(String equipo);

    @Insert
    void addMatch(Premierleague match);

    @Insert
    void addMatches(List<Premierleague> match);

    @Delete
    void deleteMatches(Premierleague premierleague);

    @Query("DELETE FROM Premierleague")
    void deleteMatches();

}
