package com.example.proyecto.ui.main;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

public class PremierLeagueViewModel extends AndroidViewModel {
    private final Application app;
    private final Appsdatabase appDatabase;
    private final PremierLeagueDao PLdao;
    private ArrayList<Premierleague> result = new ArrayList<>();


    public PremierLeagueViewModel(Application application) {
        super(application);

        this.app = application;
        this.appDatabase = Appsdatabase.getDatabase(
        (this.getApplication()));
        this.PLdao = appDatabase.getMatchesDao();

    }

    public LiveData<List<Premierleague>> getMatches() {
        return PLdao.getMatches();
    }


    public LiveData<List<Premierleague>> getEquipoSelecionado() {
        return PLdao.getEquipo(MainFragment.getEquipoSelect());
    }


    public void reload() {
        // do async operation to fetch users
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }
    private class RefreshDataTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Premierleaguedbapi api = new Premierleaguedbapi();
            if(result.size()==0) {
                result = api.Getmatches();
            }

            PLdao.deleteMatches();
            PLdao.addMatches(result);

            return null;
        }

    }

}


