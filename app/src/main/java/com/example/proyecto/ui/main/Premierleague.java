package com.example.proyecto.ui.main;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import kotlin.experimental.ExperimentalTypeInference;

@Entity
public class Premierleague implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String round;
    private String date;
    private int ScoreTeam1;
    private int ScoreTeam2;

    @ColumnInfo(name = "team1")
    private String team1;
    @ColumnInfo(name = "team2")
    private String team2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScoreTeam1() {
        return ScoreTeam1;
    }

    public void setScoreTeam1(int scoreTeam1) {
        ScoreTeam1 = scoreTeam1;
    }

    public int getScoreTeam2() {
        return ScoreTeam2;
    }

    public void setScoreTeam2(int scoreTeam2) {
        ScoreTeam2 = scoreTeam2;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    @Override
    public String toString() {
        return "Premierleague{" +
                "round='" + round + '\'' +
                ", date='" + date + '\'' +
                ", ScoreTeam1=" + ScoreTeam1 +
                ", ScoreTeam2=" + ScoreTeam2 +
                ", team1='" + team1 + '\'' +
                ", team2='" + team2 + '\'' +
                '}';
    }
}
