package com.example.proyecto.ui.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.proyecto.R;
import com.example.proyecto.databinding.LvPremRowBindingImpl;
import com.example.proyecto.databinding.MainFragment2Binding;
import com.example.proyecto.databinding.MainFragmentBinding;

import java.util.List;

public class Premierleagueadapter extends ArrayAdapter<Premierleague> {

    private LvPremRowBindingImpl binding;
    public Premierleagueadapter(Context context, int resource, List<Premierleague> objects) {
        super(context, resource, objects);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Obtenim l'objecte en la possició corresponent
        Premierleague match = getItem(position);
        Log.w("XXXX", match.toString());

        // Mirem a veure si la View s'està reutilitzant, si no es així "inflem" la View
        // https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView#row-view-recycling
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_prem_row, parent, false);
        }

        // Unim el codi en les Views del Layout
       TextView tvTitle = convertView.findViewById(R.id.lvDate);
        TextView tvteam1 = convertView.findViewById(R.id.lvteam1);
        TextView tvteam2 = convertView.findViewById(R.id.lvteam2);


      /*  binding.lvDate.setText(match.getDate());
        binding.lvteam1.setText(match.getTeam1());
        binding.lvteam2.setText(match.getTeam2());

       */

        ImageView image = convertView.findViewById(R.id.Team1image);
        ImageView image2 = convertView.findViewById(R.id.team2image);

        // Fiquem les dades dels objectes (provinents del JSON) en el layout

        tvTitle.setText(match.getDate());
        tvteam1.setText(match.getTeam1());
        tvteam2.setText(match.getTeam2());


        for (int i = 0; i < 380; i++) {
            if (match.getTeam1().equals("West Bromwich Albion FC")) {
                Glide.with(getContext()).load(
                        R.drawable.albion
                ).into(image);
            } else if (match.getTeam1().equals("Arsenal FC")) {
                Glide.with(getContext()).load(
                        R.drawable.arsenal
                ).into(image);
            } else if (match.getTeam1().equals("Aston Villa FC")) {
                Glide.with(getContext()).load(
                        R.drawable.astonvilla
                ).into(image);
            } else if (match.getTeam1().equals("Brighton & Hove Albion FC")) {
                Glide.with(getContext()).load(
                        R.drawable.brighton
                ).into(image);
            } else if (match.getTeam1().equals("Burnley FC")) {
                Glide.with(getContext()).load(
                        R.drawable.burnley
                ).into(image);
            } else if (match.getTeam1().equals("Chelsea FC")) {
                Glide.with(getContext()).load(
                        R.drawable.chelsea
                ).into(image);
            } else if (match.getTeam1().equals("Everton FC")) {
                Glide.with(getContext()).load(
                        R.drawable.everton
                ).into(image);
            } else if (match.getTeam1().equals("Crystal Palace FC")) {
                Glide.with(getContext()).load(
                        R.drawable.crystalpalace
                ).into(image);
            } else if (match.getTeam1().equals("Fulham FC")) {
                Glide.with(getContext()).load(
                        R.drawable.fulham
                ).into(image);
            } else if (match.getTeam1().equals("Leeds United FC")) {
                Glide.with(getContext()).load(
                        R.drawable.leeds
                ).into(image);
            } else if (match.getTeam1().equals("Leicester City FC")) {
                Glide.with(getContext()).load(
                        R.drawable.leicester
                ).into(image);
            } else if (match.getTeam1().equals("Liverpool FC")) {
                Glide.with(getContext()).load(
                        R.drawable.liverpool
                ).into(image);
            } else if (match.getTeam1().equals("Manchester City FC")) {
                Glide.with(getContext()).load(
                        R.drawable.manchestercity
                ).into(image);
            } else if (match.getTeam1().equals("Manchester United FC")) {
                Glide.with(getContext()).load(
                        R.drawable.manchesterunited
                ).into(image);
            } else if (match.getTeam1().equals("Newcastle United FC")) {
                Glide.with(getContext()).load(
                        R.drawable.newcastle
                ).into(image);
            } else if (match.getTeam1().equals("Sheffield United FC")) {
                Glide.with(getContext()).load(
                        R.drawable.shefieldunited
                ).into(image);
            } else if (match.getTeam1().equals("Southampton FC")) {
                Glide.with(getContext()).load(
                        R.drawable.southampton
                ).into(image);
            } else if (match.getTeam1().equals("Tottenham Hotspur FC")) {
                Glide.with(getContext()).load(
                        R.drawable.tottenham
                ).into(image);
            } else if (match.getTeam1().equals("West Ham United FC")) {
                Glide.with(getContext()).load(
                        R.drawable.westham
                ).into(image);
            } else if (match.getTeam1().equals("Wolverhampton Wanderers FC")) {
                Glide.with(getContext()).load(
                        R.drawable.wolverhampton
                ).into(image);
            } else {
                Glide.with(getContext()).load(
                        R.drawable.pl
                ).into(image);
            }
            if (match.getTeam2().equals("Wolverhampton Wanderers FC")) {
                Glide.with(getContext()).load(
                        R.drawable.wolverhampton
                ).into(image2);
            } else if (match.getTeam2().equals("Arsenal FC")) {
                Glide.with(getContext()).load(
                        R.drawable.arsenal
                ).into(image2);
            } else if (match.getTeam2().equals("Aston Villa FC")) {
                Glide.with(getContext()).load(
                        R.drawable.astonvilla
                ).into(image2);
            } else if (match.getTeam2().equals("Brighton & Hove Albion FC")) {
                Glide.with(getContext()).load(
                        R.drawable.brighton
                ).into(image2);
            } else if (match.getTeam2().equals("Burnley FC")) {
                Glide.with(getContext()).load(
                        R.drawable.burnley
                ).into(image2);
            } else if (match.getTeam2().equals("Chelsea FC")) {
                Glide.with(getContext()).load(
                        R.drawable.chelsea
                ).into(image2);
            } else if (match.getTeam2().equals("Everton FC")) {
                Glide.with(getContext()).load(
                        R.drawable.everton
                ).into(image2);
            } else if (match.getTeam2().equals("Crystal Palace FC")) {
                Glide.with(getContext()).load(
                        R.drawable.crystalpalace
                ).into(image2);
            } else if (match.getTeam2().equals("Fulham FC")) {
                Glide.with(getContext()).load(
                        R.drawable.fulham
                ).into(image2);
            } else if (match.getTeam2().equals("Leeds United FC")) {
                Glide.with(getContext()).load(
                        R.drawable.leeds
                ).into(image2);
            } else if (match.getTeam2().equals("Leicester City FC")) {
                Glide.with(getContext()).load(
                        R.drawable.leicester
                ).into(image2);
            } else if (match.getTeam2().equals("Liverpool FC")) {
                Glide.with(getContext()).load(
                        R.drawable.liverpool
                ).into(image2);
            } else if (match.getTeam2().equals("Manchester City FC")) {
                Glide.with(getContext()).load(
                        R.drawable.manchestercity
                ).into(image2);
            } else if (match.getTeam2().equals("Manchester United FC")) {
                Glide.with(getContext()).load(
                        R.drawable.manchesterunited
                ).into(image2);
            } else if (match.getTeam2().equals("Newcastle United FC")) {
                Glide.with(getContext()).load(
                        R.drawable.newcastle
                ).into(image2);
            } else if (match.getTeam2().equals("Sheffield United FC")) {
                Glide.with(getContext()).load(
                        R.drawable.shefieldunited
                ).into(image2);
            } else if (match.getTeam2().equals("Southampton FC")) {
                Glide.with(getContext()).load(
                        R.drawable.southampton
                ).into(image2);
            } else if (match.getTeam2().equals("Tottenham Hotspur FC")) {
                Glide.with(getContext()).load(
                        R.drawable.tottenham
                ).into(image2);
            } else if (match.getTeam2().equals("West Ham United FC")) {
                Glide.with(getContext()).load(
                        R.drawable.westham
                ).into(image2);
            } else if (match.getTeam2().equals("West Bromwich Albion FC")) {
                Glide.with(getContext()).load(
                        R.drawable.albion
                ).into(image2);
            } else {
                Glide.with(getContext()).load(
                        R.drawable.pl
                ).into(image);
            }
        }


            // Retornem la View replena per a mostrar-la
            return convertView;
        }


    }
