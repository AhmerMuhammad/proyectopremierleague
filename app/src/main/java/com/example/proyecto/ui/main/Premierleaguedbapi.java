package com.example.proyecto.ui.main;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class Premierleaguedbapi {

    private ArrayList<Premierleague> equipos=new ArrayList<>();
    private Premierleague pl;


    private final String BASE_URL = "https://raw.githubusercontent.com/";

    ArrayList<Premierleague> Getmatches() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("openfootball")
                .appendPath("football.json")
                .appendPath("master")
                .appendPath("2020-21")
                .appendPath("en.1.json")
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }



    private ArrayList<Premierleague> doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return equipos;
    }

    private ArrayList<Premierleague> processJson(String jsonResponse) {
        equipos=new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonPrem = data.getJSONArray("matches");

            for (int i = 0; i < jsonPrem.length(); i++) {
                JSONObject jsonPremier = jsonPrem.getJSONObject(i);

               pl = new Premierleague();
                pl.setRound(jsonPremier.getString("round"));
                pl.setDate(jsonPremier.getString("date"));
                pl.setTeam1(jsonPremier.getString("team1"));
                pl.setTeam2(jsonPremier.getString("team2"));
                if (jsonPremier.has("score")){
                    pl.setScoreTeam1(jsonPremier.getJSONObject("score").getJSONArray("ft").getInt(0));
                    pl.setScoreTeam2(jsonPremier.getJSONObject("score").getJSONArray("ft").getInt(1));

                }
                else {
                    pl.setScoreTeam1(-1);
                    pl.setScoreTeam2(-1);

                }

                equipos.add(pl);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return equipos;
    }
}

