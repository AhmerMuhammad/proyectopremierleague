package com.example.proyecto.ui.main;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Premierleague> selected = new MutableLiveData<Premierleague>();

    public void select(Premierleague match) {
        selected.setValue(match);
    }

    public LiveData<Premierleague> getSelected() {
        return selected;
    }
}