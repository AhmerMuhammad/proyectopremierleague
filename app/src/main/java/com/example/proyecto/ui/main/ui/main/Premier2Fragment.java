package com.example.proyecto.ui.main.ui.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.renderscript.ScriptGroup;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.proyecto.R;
import com.example.proyecto.databinding.MainFragment2Binding;
import com.example.proyecto.databinding.MainFragmentBinding;
import com.example.proyecto.ui.main.MainFragment;
import com.example.proyecto.ui.main.Premierleague;
import com.example.proyecto.ui.main.SharedViewModel;

public class Premier2Fragment extends Fragment {

    private MainViewModel2 mViewModel;
    private View view;
    private ImageView Team1;
    private ImageView Team2;

    private MainFragment2Binding binding;


    public static Premier2Fragment newInstance() {
        return new Premier2Fragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = MainFragment2Binding.inflate(inflater);
        view = binding.getRoot();

        Intent i = getActivity().getIntent();

        if (i != null) {
            Premierleague pl = (Premierleague) i.getSerializableExtra("Match");
            System.out.println(pl+"+6+++++++++++++++++++++++++++++++");
            if (pl != null) {
                updateUi(pl);
            }

        }

        SharedViewModel sharedModel = ViewModelProviders.of(
                getActivity()
        ).get(SharedViewModel.class);
        sharedModel.getSelected().observe(getViewLifecycleOwner(), new Observer<Premierleague>() {
            @Override
            public void onChanged(@Nullable Premierleague match) {
                updateUi(match);
            }
        });

        return view;
    }


    private void updateUi(Premierleague match) {
        Log.d("Match", match.toString());

        Team1 = view.findViewById(R.id.Team1imageeeeee);
        Team2 =  view.findViewById(R.id.Team2imageeeeee);

        binding.Team1nameee.setText(match.getTeam1());
        binding.Team2nameee.setText(match.getTeam2());

        binding.Date.setText(match.getDate());
        binding.Round.setText(match.getRound());

        System.out.println(match+"matchhhhhhhhhhhhhhhhhhhhhhhhh");
        if (match.getScoreTeam1()==-1) {
            binding.Score1.setText(String.valueOf(match.getScoreTeam1()));
            binding.Score2.setText(String.valueOf(match.getScoreTeam2()));

        }
        else{
            binding.Score1.setText(String.valueOf(match.getScoreTeam1()));
            binding.Score2.setText(String.valueOf(match.getScoreTeam2()));
        }



        if (match.getTeam1().equals("West Bromwich Albion FC")) {
            Glide.with(getContext()).load(
                    R.drawable.albion
            ).into(binding.Team1imageeeeee);

            System.out.println(Team1+"ganggggggggggggggggggggggggggggggggggggggggggggggggggg");
        } else if (match.getTeam1().equals("Arsenal FC")) {
            Glide.with(getContext()).load(
                    R.drawable.arsenal
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Aston Villa FC")) {
            Glide.with(getContext()).load(
                    R.drawable.astonvilla
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Brighton & Hove Albion FC")) {
            Glide.with(getContext()).load(
                    R.drawable.brighton
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Burnley FC")) {
            Glide.with(getContext()).load(
                    R.drawable.burnley
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Chelsea FC")) {
            Glide.with(getContext()).load(
                    R.drawable.chelsea
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Everton FC")) {
            Glide.with(getContext()).load(
                    R.drawable.everton
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Crystal Palace FC")) {
            Glide.with(getContext()).load(
                    R.drawable.crystalpalace
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Fulham FC")) {
            Glide.with(getContext()).load(
                    R.drawable.fulham
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Leeds United FC")) {
            Glide.with(getContext()).load(
                    R.drawable.leeds
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Leicester City FC")) {
            Glide.with(getContext()).load(
                    R.drawable.leicester
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Liverpool FC")) {
            Glide.with(getContext()).load(
                    R.drawable.liverpool
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Manchester City FC")) {
            Glide.with(getContext()).load(
                    R.drawable.manchestercity
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Manchester United FC")) {
            Glide.with(getContext()).load(
                    R.drawable.manchesterunited
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Newcastle United FC")) {
            Glide.with(getContext()).load(
                    R.drawable.newcastle
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Sheffield United FC")) {
            Glide.with(getContext()).load(
                    R.drawable.shefieldunited
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Southampton FC")) {
            Glide.with(getContext()).load(
                    R.drawable.southampton
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Tottenham Hotspur FC")) {
            Glide.with(getContext()).load(
                    R.drawable.tottenham
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("West Ham United FC")) {
            Glide.with(getContext()).load(
                    R.drawable.westham
            ).into(binding.Team1imageeeeee);
        } else if (match.getTeam1().equals("Wolverhampton Wanderers FC")) {
            Glide.with(getContext()).load(
                    R.drawable.wolverhampton
            ).into(binding.Team1imageeeeee);
        } else {
            Glide.with(getContext()).load(
                    R.drawable.pl
            ).into(binding.Team1imageeeeee);
        }
        if (match.getTeam2().equals("Wolverhampton Wanderers FC")) {
            Glide.with(getContext()).load(
                    R.drawable.wolverhampton
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Arsenal FC")) {
            Glide.with(getContext()).load(
                    R.drawable.arsenal
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Aston Villa FC")) {
            Glide.with(getContext()).load(
                    R.drawable.astonvilla
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Brighton & Hove Albion FC")) {
            Glide.with(getContext()).load(
                    R.drawable.brighton
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Burnley FC")) {
            Glide.with(getContext()).load(
                    R.drawable.burnley
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Chelsea FC")) {
            Glide.with(getContext()).load(
                    R.drawable.chelsea
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Everton FC")) {
            Glide.with(getContext()).load(
                    R.drawable.everton
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Crystal Palace FC")) {
            Glide.with(getContext()).load(
                    R.drawable.crystalpalace
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Fulham FC")) {
            Glide.with(getContext()).load(
                    R.drawable.fulham
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Leeds United FC")) {
            Glide.with(getContext()).load(
                    R.drawable.leeds
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Leicester City FC")) {
            Glide.with(getContext()).load(
                    R.drawable.leicester
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Liverpool FC")) {
            Glide.with(getContext()).load(
                    R.drawable.liverpool
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Manchester City FC")) {
            Glide.with(getContext()).load(
                    R.drawable.manchestercity
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Manchester United FC")) {
            Glide.with(getContext()).load(
                    R.drawable.manchesterunited
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Newcastle United FC")) {
            Glide.with(getContext()).load(
                    R.drawable.newcastle
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Sheffield United FC")) {
            Glide.with(getContext()).load(
                    R.drawable.shefieldunited
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Southampton FC")) {
            Glide.with(getContext()).load(
                    R.drawable.southampton
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("Tottenham Hotspur FC")) {
            Glide.with(getContext()).load(
                    R.drawable.tottenham
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("West Ham United FC")) {
            Glide.with(getContext()).load(
                    R.drawable.westham
            ).into(binding.Team2imageeeeee);
        } else if (match.getTeam2().equals("West Bromwich Albion FC")) {
            Glide.with(getContext()).load(
                    R.drawable.albion
            ).into(binding.Team2imageeeeee);
        } else {
            Glide.with(getContext()).load(
                    R.drawable.pl
            ).into(binding.Team2imageeeeee);
        }


    }

}